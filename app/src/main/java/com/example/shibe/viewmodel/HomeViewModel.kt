package com.example.shibe.viewmodel

import androidx.lifecycle.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.shibe.model.ShibeRepo
import com.example.shibe.model.local.entity.Shibe
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repo: ShibeRepo) : ViewModel() {

    val state:LiveData<ShibeState> = liveData{
        emit(ShibeState(isLoading = true))
        val shibes = repo.getShibes()
        emit(ShibeState(shibes = shibes))
    }

    data class ShibeState(
        val isLoading: Boolean = false,
        val shibes: List<Shibe> = emptyList()
    )
}