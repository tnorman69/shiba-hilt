package com.example.shibe.model

import android.content.Context
import android.util.Log
import com.example.shibe.model.local.ShibeDatabase
import com.example.shibe.model.local.entity.Shibe
import com.example.shibe.model.remote.ShibeService
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ShibeRepo @Inject constructor(val shibeService: ShibeService, @ApplicationContext context: Context) {

    val shibeDao = ShibeDatabase.getInstance(context).shibeDao()

    suspend fun getShibes(number: Int = 5) = withContext(Dispatchers.IO) {
        val cachedShibes: List<Shibe> = shibeDao.getAll()

        return@withContext cachedShibes.ifEmpty {
            val shibeUrls: List<String> = shibeService.getShibes(number)
            val shibes: List<Shibe> = shibeUrls.map { Shibe(url = it) }
            shibeDao.insert(shibes)
            return@ifEmpty shibes
        }
    }
}