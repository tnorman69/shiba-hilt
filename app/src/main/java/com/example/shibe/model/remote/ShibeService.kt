package com.example.shibe.model.remote

import com.example.shibe.model.response.ShibeDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {

    @GET("/api/shibes")
    suspend fun getShibes(@Query(QUERY_CATEGORY) number: Int): ShibeDTO

    companion object {
        const val BASE_URL = "https://shibe.online"
        private const val QUERY_CATEGORY = "count"
    }
}