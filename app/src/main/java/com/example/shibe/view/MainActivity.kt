package com.example.shibe.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.shibe.R
import dagger.hilt.EntryPoint
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.HiltAndroidApp

@AndroidEntryPoint

class MainActivity : AppCompatActivity(R.layout.activity_main)