package com.example.shibe.view

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ShibesApplication :Application()